const express = require('express');
const path    = require('path');
const PORT    = process.env.PORT || 80;
const app     = express();

app.use(express.static(path.join(__dirname, '/build')));

app.get('*', (req, res) => {
    res.status(200).sendFile(path.join(__dirname + '/build/index.html'))
})

app.listen(PORT);