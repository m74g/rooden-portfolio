import * as Icons from 'simple-icons';

import * as firebase from "firebase/app";
import "firebase/analytics";
import "firebase/firestore"

import './src/style.css';

const skillsRootEl      = document.querySelector('.skills');
const petsRootEl        = document.querySelector('.pets');
const blogRootEl        = document.querySelector('.blog');
const commercialsRootEl = document.querySelector('.commercials');
const socialsRootEl     = document.querySelector('.socials');
const copyrightRootEl   = document.querySelector('.copyright');

if (!firebase.apps.length) {
    firebase.initializeApp({
        apiKey: "AIzaSyAnVLrML73LFPmWQE597DwC-IWLnjifFzM",
        authDomain: "roodan-dev.firebaseio.com",
        databaseURL: "https://roodan-dev.firebaseio.com",
        projectId: "roodan-dev",
        storageBucket: "roodan-dev.appspot.com",
        messagingSenderId: "1059201557354",
        appId: "1:1059201557354:web:db46c552851cf0b0d03b13",
        measurementId: "G-31CCXE51TF"
    });
}

var db = firebase.firestore();
db.collection('reg_posts').orderBy("date", "desc").get().then(function (querySnap) {
    querySnap.forEach(function (doc) {
        setBlogPosts(doc.data());
    })
})

const skills = [
    Icons["JavaScript"],
    Icons["React"],
    Icons["Node.js"],
    Icons["Webpack"],
    Icons["MongoDB"],
    Icons["Ubuntu"],
]

const pets = [
    {name: 'password_generator', link:'https://bitbucket.org/m74g/genpass/src/master/', type: 'tool'},
    {name: 'phodel', link:'https://bitbucket.org/m74g/phodel/src/master/', type: 'website'}
]

const commercials = [
    {name: 'deep2000', link:'http://deep2000.ru/', type: 'frontend', expirience: '1.5'},
    {name: 'betsoft', link:'https://betsoft.com/', type: 'gamedev', expirience: '1.5'}
]

const socials = [
    {name: 'vk', link:'https://vk.com/roodendev', type: 'vk'},
    {name: 'instagram', link:'https://www.instagram.com/_roodan/', type: 'instagram'},
    {name: 'twitter', link:'https://twitter.com/_roodan', type: 'twitter'},
    {name: 'reddit', link:'https://www.reddit.com/user/m74g', type: 'reddit'},
    {name: 'github', link:'https://github.com/r00dan', type: 'github'},
    {name: 'bitbucket', link:'https://bitbucket.org/m74g', type: 'bitbucket'}
]


let createSkillBlock = (skill) => {
    let el = document.createElement('div');
    el.className += `col s1 valign-wrapper center-align hoverable ${skill.slug} skill`;    
    el.style.backgroundColor = `#${skill.hex}`;
    el.innerHTML = skill.svg;
    return el;
}

let setSkills = () => {
    for(let s of skills) {
        let el = createSkillBlock(s);
        skillsRootEl.appendChild(el);
    }
}

let createWorkLink = (work) => {
    let el = document.createElement('a');
    el.className += `collection-item work blue-grey darken-3 teal-text text-accent-3`; 
    el.target = '_blank' 
    el.href = work.link  ;
    el.innerHTML = work.name + (work.expirience ? `<span class="new badge teal darken-2" data-badge-caption="">${work.expirience}y</span>` : '') + `<span class="new badge teal accent-4" data-badge-caption="">${work.type}</span>`;
    return el;
}

let setPetWorks = () => {
    for(let p of pets) {
        let el = createWorkLink(p);
        petsRootEl.appendChild(el);
    }
}

let setCommercialWorks = () => {
    for(let c of commercials) {
        let el = createWorkLink(c);
        commercialsRootEl.appendChild(el);
    }
}

let setSocials = () => {
    for(let s of socials) {
        let el = createWorkLink(s);
        socialsRootEl.appendChild(el);
    }
}

let createBlogPost = (post) => {
    if (!post) return false;
    let date = `${post.date.toDate().getMonth()}/${post.date.toDate().getDate()}/${post.date.toDate().getFullYear()}`;
    let el = document.createElement('li');  
    el.innerHTML = `<div class="collapsible-header blue-grey darken-3 teal-text text-accent-3">${post.name}<span class="new badge teal accent-4" data-badge-caption="">${date}</span></div>
                    <div class="collapsible-body post blue-grey darken-4 teal-text text-lighten-5"><span>${post.content}</span></div>`;
    return el;
}

let setBlogPosts = bp => {
    if (!bp) 
    {
        blogRootEl.innerHtml = '<div>There are no posts here yet :(</div>'
        return false;
    }
    let el = createBlogPost(bp);
    blogRootEl.appendChild(el);
}

copyrightRootEl.innerHTML = `© ${new Date().getFullYear()} ${window.location.hostname} All Rights Reserved <a class="grey-text text-lighten-1 right">${new Date().getTime()}</a>`;

setInterval(function() {
    copyrightRootEl.innerHTML = `© ${new Date().getFullYear()} ${window.location.hostname} All Rights Reserved <a class="grey-text text-lighten-1 right">${new Date().getTime()}</a>`;
}, 500)



M.AutoInit();
setSkills();
setBlogPosts();
setCommercialWorks();
setSocials();